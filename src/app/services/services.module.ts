import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
 
  ResolucionesService,
 
} from './services.index';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    
    ResolucionesService,
    
  ]
})
export class ServicesModule { }
