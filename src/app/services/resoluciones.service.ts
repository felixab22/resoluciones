import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PersonalModel, OrigenModel, ResolutionModel } from '../model/resoluciones.model';


@Injectable({
    providedIn: 'root'
})
export class ResolucionesService {
    api='http://localhost:8080';
    constructor(
        private http: HttpClient
    ) {

    }

    public getPersona(): Observable<any[]> {
        return this.http.get<any[]>
          (this.api + `/listaPersonas`, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
      }

      // Guardar y editar
    public saveOrUpdatePersonal(persona: PersonalModel): Observable<PersonalModel> {
        return this.http.post<any>('http://localhost:8080/SaveOpdatePersona',
            JSON.stringify(persona), { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }

    public getOrigen(): Observable<any[]> {
        return this.http.get<any[]>
          (this.api + `/listaOrigen`, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
      }

      public saveOrUpdateOrigen(origen: OrigenModel): Observable<OrigenModel> {
        return this.http.post<any>('http://localhost:8080/SaveOpdateobjOrigen',
            JSON.stringify(origen), { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }



    public saveOrUpdateResolution(origen: ResolutionModel): Observable<ResolutionModel> {
        return this.http.post<any>('http://localhost:8080/SaveOpdateResolution',
            JSON.stringify(origen), { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
    }
    public getResolutionxTipo(tipo:number): Observable<any[]> {
        return this.http.get<any[]>
          (this.api + `/listResolutionByTipo?tipo=${tipo}`, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
      }

}