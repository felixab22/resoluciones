import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';
import { PagesComponent } from './pages/pages.component';
import { BienvenidoComponent } from './pages/bienvenido/bienvenido.component';
import { PresentadoComponent } from './pages/presentado/presentado.component';
import { DerivadosComponent } from './pages/derivados/derivados.component';
import { DecanalComponent } from './pages/decanal/decanal.component';
import { PendientesComponent } from './pages/pendientes/pendientes.component';
import { ConsejoComponent } from './pages/consejo/consejo.component';
import { PdfComponent } from './pages/pdf/pdf.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: LoginComponent },
  {
    path: 'Resoluciones',
    component: PagesComponent,
    children:
      [
        { path: 'bienvenido', component: BienvenidoComponent },
        { path: 'Presentado', component: PresentadoComponent },
        { path: 'Usuarios', component: DerivadosComponent },
        { path: 'Resol-decanal', component: DecanalComponent },
        { path: 'Resol-consejo', component: ConsejoComponent },
        { path: 'Pendientes', component: PendientesComponent },
        { path: 'Pdf', component: PdfComponent },
        // { path: '**', component: NopageComponent }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
