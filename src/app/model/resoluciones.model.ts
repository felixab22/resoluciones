export class DocumentosModel {
    id: number;
    code: string;
    descripcion: string;
    fechaPresentada: string;
    nombre: string;
    apellidos: string;
    dni: number;
    codigoU: number;
    direccion: string;
};
export class PersonalModel {
    idpersona: number;
    nombre: string;
    apellidos: string;
    dni: number;
    codeuniversidad: number;
    direction: string;
    
};
export class OrigenModel {
    idorigen: number;
    codedocorigen: string;
    description: string;
    fechapresen: string;
    idpersona: number;
};
export class ResolutionModel {
    idresolution: number;
    coderesolution: string;
    fechacreation: string;
    visto: string;
    presentado: string;
    considerando: string;
    resuelve: string;
    cc: string;
    tipo: number;
    estado: boolean;
    idorigen: number;
};