import { Component, OnInit } from '@angular/core';
import { ResolucionesService } from 'src/app/services/services.index';
import { Router } from '@angular/router';
declare var swal: any;
@Component({
  selector: 'app-decanal',
  templateUrl: './decanal.component.html',
  styleUrls: ['./decanal.component.scss']
})
export class DecanalComponent implements OnInit {
  facultades: { id: number; valor: string; }[];
  headElements: string[];
  term = '';
  decanales: { id: number; code: string; fechaemition: string; codeOrigen: string; visto: string; presentado: string; considerando: string; resuelve: string; cc: string; }[];
  constructor(
    private _resolSrv: ResolucionesService,
    private router: Router
  ) {
    this.headElements = ['Codigo decanal', 'Fecha', 'Código', 'Visto', 'Presentado', 'Considerando', 'Resuelve', 'CC']
    this.decanales = [
      // {
      //   id: 1, code: '045–2019-FIMGC-D',
      //   fechaemition: '01/02/2019',
      //   codeOrigen: '108-2019-FIMGC',
      //   visto: 'visto que',
      //   presentado: 'esta es la presentacion',
      //   considerando: 'secretaria',
      //   resuelve: 'resuleve que',
      //   cc: 'con copia'
      // },
      // {
      //   id: 2, code: '046–2019-FIMGC-D',
      //   fechaemition: '13/02/2019',
      //   codeOrigen: '119-2019-FIMGC',
      //   visto: 'visto que',
      //   presentado: 'esta es la presentacion',
      //   considerando: 'secretaria',
      //   resuelve: 'resuleve que',
      //   cc: 'con copia'
      // },
      // {
      //   id: 3, code: '047–2019-FIMGC-D',
      //   fechaemition: '21/02/2019',
      //   codeOrigen: '130-2019-FIMGC',
      //   visto: 'visto que',
      //   presentado: 'esta es la presentacion',
      //   considerando: 'secretaria',
      //   resuelve: 'resuleve que',
      //   cc: 'con copia'
      // },
      // {
      //   id: 3, code: '048–2019-FIMGC-D',
      //   fechaemition: '17/03/2019',
      //   codeOrigen: '141-2019-FIMGC',
      //   visto: 'visto que',
      //   presentado: 'esta es la presentacion',
      //   considerando: 'secretaria',
      //   resuelve: 'resuleve que',
      //   cc: 'con copia'
      // },
      {
        id: 5, code: '049–2019-FIMGC-D',
        fechaemition: '21/03/2019',
        codeOrigen: '187-2019-FIMGC',
        visto: ' Visto, la solicitud Nº 287 de fecha 06 de julio 2019, presentado por el Bachiller VICTOR HUGO YARANGA PADRO de la Escuela Profesional de Ingeniería Civil, mediante el cual solicita la aprobación de plan de tesis; y',
        presentado: 'Que Mediante Dictamen Nº 052-2019-UNSCH7FIMGC/JBP/EL/ALF de fecha 04 de diciembre de 2019 la comisión dictaminadora conformado por el Ing. Jaime L. Bendezu Prado(presidente), Msc. Ing. Adolfo Linares Flores (miembro) y Ing. Edward León Palacios(miembro) representantes de la Escuela Profesional de Ingeniería Civil, toman conocimiento del plan de tesos presentado por el recurrente y remiten el dictamen de aprobación de Plan de Tesis Titulado “OBTIMIZACIÓN EN EL DISEÑO DE REDES ALCANTARILLADO MEDIENTE PROGRAMACIÓN DINAMICA”, adjunto para ello el ejemplar del referido Plan y el Dictamen correspondiente quedando apto para la ejecución de la tesis;',
        considerando: 'Que, con Memorando Múltiple Nº 187-2019-FIMGC de fecha 14 de julio del 2017 la Facultad de Ingeniería de Minas Geología y Civil deriva el plan de tesis del presidente de la comisión dictaminadora el expediente Nº 287 del Bachiller VICTOR HUGO YARANGA PADRO para su dictamen por escrito en concordancia en lo dispuesto en el procedimiento de la aprobación del Plan de tesis de acuerdo a los Artículos 16º al 21º del plan Curricular 2004 de la Escuela Profesional de Ingeniería Civil; ',
        resuelve: 'resuleve que',
        cc: 'Distribución Expediente, Bachiller ,Comisión Dictaminadora (03) ,Archivo'
      },
    ];
  }

  ngOnInit() {



    this.facultades = [
      { id: 1, valor: 'FIMGC' },
      { id: 2, valor: 'FCA' },
      { id: 3, valor: 'FCB' },
      { id: 4, valor: 'FCE' },
      { id: 5, valor: 'FCEAC' },
      { id: 6, valor: 'FCS' },
      { id: 7, valor: 'FDCP' },
      { id: 8, valor: 'FIQM' },
      { id: 9, valor: 'FCS' },
    ];
  }
  SelecActivosFacultades(facultad) {
    console.log(facultad);

  }
  eliminar(item) {
    swal({
      title: "¿Eliminar el resolución decanal?",
      text: "Usted esta por eliminar temporalmente!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.decanales = null;
          swal("Se elimino correctamente!", {
            icon: "success",
          });
        } else {
          swal("Cancelado!");
        }
      });
  }
  GenerarPDF() {
    this.router.navigate(['/Resoluciones/Pdf']);
  }

}
