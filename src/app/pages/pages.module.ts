import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { SharedModule } from '../shared/shared.module';

import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';
import { PresentadoComponent } from './presentado/presentado.component';
import { DerivadosComponent } from './derivados/derivados.component';
import { DecanalComponent } from './decanal/decanal.component';
import { PendientesComponent } from './pendientes/pendientes.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { ConsejoComponent } from './consejo/consejo.component'; // <-- import the module
import { ServicesModule } from '../services/services.module';
import { PdfComponent } from './pdf/pdf.component';


@NgModule({
  declarations: [
    PagesComponent,
    BienvenidoComponent,
    PresentadoComponent,
    DerivadosComponent,
    DecanalComponent,
    PendientesComponent,
    ConsejoComponent,
    PdfComponent   
    ],
  imports: [
    MDBBootstrapModule.forRoot(),
    RouterModule,
    SharedModule,
    Ng2SearchPipeModule,
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ChartsModule, WavesModule,
    NgxPaginationModule,
    ServicesModule
  ],
  exports: [
    BienvenidoComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PagesModule { }
