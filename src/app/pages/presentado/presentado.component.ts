import { Component, OnInit } from '@angular/core';
import { DocumentosModel, PersonalModel, OrigenModel, ResolutionModel } from 'src/app/model/resoluciones.model';
import { Router } from '@angular/router';
import { ResolucionesService } from 'src/app/services/services.index';
declare var swal : any;
@Component({
  selector: 'app-presentado',
  templateUrl: './presentado.component.html',
  styleUrls: ['./presentado.component.scss']
})
export class PresentadoComponent implements OnInit {
  dirigidos: { id: number; valor: string; }[];
  Requerimientos: { id: number; valor: string; }[];
  selectedrequerimiento: string[] = [];
  igualar1: any;
  codigomuestra = '';
NewResolition:ResolutionModel;
  newOrigen: OrigenModel;
  term = '';
  headElements: string[];
  tipedocumento: string;
  titulo1: string;
  dnibuscar: number;
  Allpersonas: PersonalModel[];
  pruebaPer: PersonalModel;
  documentos: OrigenModel[];
  constructor(
    private router: Router,
    private _resolSrv:ResolucionesService
    ) {     
      this.newOrigen= new OrigenModel();
      this.pruebaPer= new PersonalModel();      
      this.NewResolition= new ResolutionModel();      

  }

  ngOnInit() {

    this.Allpersonas = JSON.parse( localStorage.getItem('personas'));
    this.dirigidos = [
      { id: 10, valor: 'Decano' },
      { id: 11, valor: 'Rector' },
      { id: 12, valor: 'Secretario' }
    ];
    this.headElements = ['Código', 'Descripción', 'Fecha', 'Personal', 'DNI', 'Código', 'Dirección'];
    this.listarOrigen();
  }


  listarOrigen() {
    this._resolSrv.getOrigen().subscribe(res => {
      this.documentos = res;
      // console.log(res);
      
    })
  }
  editarOrigen(document, basicModal3) {
    this.titulo1 = 'EDITAR';
    basicModal3.show();
    this.newOrigen = document;
    console.log(document);

  }
  BuscarporDNI() {
    console.log(this.Allpersonas);
    for(let item of this.Allpersonas) {
      if(item.dni === this.dnibuscar) {
        this.newOrigen.idpersona = item.idpersona;
        this.pruebaPer = item;
        console.log(this.pruebaPer);
      } else {
        console.log(this.dnibuscar);  
        
        
      }
      
      
    }
  }
  
    EscogerTipo(basicModal7, basicModal8, tipo: string) {
    console.log(tipo);
    if (tipo === '1') {
      this.tipedocumento = 'resolución decanal';
      this.NewResolition.tipo = parseInt(tipo);
    } else {
      this.tipedocumento = 'consejo de facultad';
      this.NewResolition.tipo = parseInt(tipo);
    }
    basicModal7.show()
    basicModal8.hide()
  }
  eliminar(item) {

    swal({
      title: "¿Eliminar el documento de origen?",
      text: "Usted esta por eliminar temporalmente!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Se elimino correctamente!", {
          icon: "success",
        });
      } else {
        swal("Cancelado!");
      }
    });
  }
  SaveOrUpdateOrigen(basicModal3) {
    this._resolSrv.saveOrUpdateOrigen(this.newOrigen).subscribe(res=> {
      console.log(res);  
      swal({
        title: "Guardado!",
        text: "Guardado satisfactoriamente!",
        icon: "success",
        button: "aceptar!",
      }); 
      this.listarOrigen();    
      basicModal3.hide();
      console.log(this.newOrigen);
    })
    
  }
  GenerarResolDecanal(codigo, basicModal7) {
    console.log(codigo);
    this.codigomuestra = codigo.codedocorigen;
    this.NewResolition.estado = false;
    this.NewResolition.idorigen = codigo.idorigen;    
    basicModal7.show();
  }
  SaveOrUpdateResolDecanal(basicModal7) {
    this._resolSrv.saveOrUpdateResolution(this.NewResolition).subscribe(res => {
      swal("Click on either the button or outside the modal.")
      .then(() => {        
        basicModal7.hide();
        this.router.navigate(['/Resoluciones/Resol-decanal'])
      });
    })
  }
  nuevoDocOrigen(basicModal3)  {
    this.titulo1 = 'NUEVO'
    basicModal3.show();
    this.newOrigen= new OrigenModel();
  }

}
