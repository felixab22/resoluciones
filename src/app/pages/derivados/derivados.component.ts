import { Component, OnInit } from '@angular/core';
import { ResolucionesService } from 'src/app/services/services.index';
import { PersonalModel } from 'src/app/model/resoluciones.model';
declare var swal: any;
@Component({
  selector: 'app-derivados',
  templateUrl: './derivados.component.html',
  styleUrls: ['./derivados.component.scss']
})
export class DerivadosComponent implements OnInit {
  newPerona: PersonalModel;
  allPersonas: PersonalModel[];
  term = '';
  headElements: string[];
  personas: { id: number; apellidos: string; nombre: string; dni: number; codigoU: number; direccion: string; }[];
  constructor(
    private _resolSrv: ResolucionesService,
    ) {
      this.newPerona = new PersonalModel();
    this.headElements = ['Nombre', 'Apellidos', 'DNI', 'Codigo Universitario', 'Dirección']
    this.personas = [
      { id: 1, apellidos: 'Garcia Caceres', nombre: 'Christian', dni: 26578954, codigoU: 27089545, direccion: 'Jr. 9 de Diciembre Nº121' },
      { id: 2, apellidos: 'Ayala Bellido', nombre: 'Julio', dni: 48965782, codigoU: 27129657, direccion: 'Jr. Mariano Melgar Nº121' },
      { id: 3, apellidos: 'Carrasco Castro', nombre: 'Carlos', dni: 23965783, codigoU: 27149631, direccion: 'Jr. 28 de Julio Nº110' },
      { id: 4, apellidos: 'Herrera Hinostroza', nombre: 'Henry', dni: 11965722, codigoU: 27159600, direccion: 'Jr. 2 de Mayo Nº410' }
    ]
  }

  ngOnInit() {
    this.imprimirPersonas();
  }

  SaveOrUpdatePersona(basicModal9) {
    this._resolSrv.saveOrUpdatePersonal(this.newPerona).subscribe(res=> {
      console.log(res);
      basicModal9.hide();   
      swal({
        title: "Guardado!",
        text: "La persona a guardado satisfactoriamente!",
        icon: "success",
        button: "aceptar!",
      });   
    });
    this.imprimirPersonas();
  }
  imprimirPersonas() {
    this._resolSrv.getPersona().subscribe(res=> {
      this.allPersonas = res;
      localStorage.setItem('personas',JSON.stringify(res));      
    });
  }

}
