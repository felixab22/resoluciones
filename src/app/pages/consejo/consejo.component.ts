import { Component, OnInit } from '@angular/core';
import { ResolucionesService } from 'src/app/services/services.index';
declare var swal: any;
@Component({
  selector: 'app-consejo',
  templateUrl: './consejo.component.html',
  styleUrls: ['./consejo.component.scss']
})
export class ConsejoComponent implements OnInit {
  // decanales: { id: number; code: string; fechaemition: string; codeOrigen: string; content: string; cc: string; }[];
  headElements: string[];
  facultades: { id: number; valor: string; }[];
  term = '';
  decanales: { id: number; code: string; fechaemition: string; codeOrigen: string; visto: string; presentado: string; considerando: string; resuelve: string; cc: string; }[];
  constructor(
    private _resolSrv: ResolucionesService,
  ) {
    this.headElements = ['Codigo decanal', 'Fecha', 'Código', 'Visto', 'Presentado', 'Considerando', 'Resuelve', 'CC']
    this.decanales = [
      { id: 1, code: '001', fechaemition: '17/07/2019', codeOrigen: '004-FIMGC', visto: 'visto que', presentado: 'esta es la presentacion', considerando: 'secretaria', resuelve: 'resuleve que', cc: 'con copia' },
      { id: 2, code: '002', fechaemition: '19/07/2019', codeOrigen: '004-FIMGC', visto: 'visto que', presentado: 'esta es la presentacion', considerando: 'secretaria', resuelve: 'resuleve que', cc: 'con copia' },
      { id: 3, code: '003', fechaemition: '20/07/2019', codeOrigen: '004-FIMGC', visto: 'visto que', presentado: 'esta es la presentacion', considerando: 'secretaria', resuelve: 'resuleve que', cc: 'con copia' },
      { id: 3, code: '004', fechaemition: '20/07/2019', codeOrigen: '004-FIMGC', visto: 'visto que', presentado: 'esta es la presentacion', considerando: 'secretaria', resuelve: 'resuleve que', cc: 'con copia' },
      { id: 5, code: '005', fechaemition: '20/07/2019', codeOrigen: '004-FIMGC', visto: 'visto que', presentado: 'esta es la presentacion', considerando: 'secretaria', resuelve: 'resuleve que', cc: 'con copia' },
    ];
    this.facultades = [
      { id: 1, valor: 'FIMGC' },
      { id: 2, valor: 'FCA' },
      { id: 3, valor: 'FCB' },
      { id: 4, valor: 'FCE' },
      { id: 5, valor: 'FCEAC' },
      { id: 6, valor: 'FCS' },
      { id: 7, valor: 'FDCP' },
      { id: 8, valor: 'FIQM' },
      { id: 9, valor: 'FCS' },
    ];
  }
  SelecActivosFacultades(facultad) {
    console.log(facultad);

  }
  eliminar(item) {
    swal({
      title: "¿Eliminar el resolución de C.F.?",
      text: "Usted esta por eliminar temporalmente!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          this.decanales = null;
          swal("Se elimino correctamente!", {
            icon: "success",
          });
        } else {
          swal("Cancelado!");
        }
      });
  }


  ngOnInit() {
    // this._resolSrv.getResolutionxTipo(0).subscribe(res => {
    //   this.decanales = res;
    //   console.log(res);

    // });
  }

}
