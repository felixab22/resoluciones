import { Component, OnInit } from '@angular/core';
import { ResolucionesService } from 'src/app/services/services.index';

@Component({
  selector: 'app-bienvenido',
  templateUrl: './bienvenido.component.html',
  styleUrls: ['./bienvenido.component.scss']
})
export class BienvenidoComponent implements OnInit {

  constructor(
    private _resolSrv: ResolucionesService,
  ) { }

  ngOnInit() {
  }
  imprimirPersonas() {
    this._resolSrv.getPersona().subscribe(res=> {
      localStorage.setItem('personas',JSON.stringify(res));
      
      
    });
  }

}
