import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.component.html',
  styleUrls: ['./pendientes.component.scss']
})
export class PendientesComponent implements OnInit {
  headElements: any;
  pendientes: { id: number; code: string; tipo: string; fechaPendiente: string; dirigidoA: string; acction: boolean; }[];
  term = '';
  p = 1;
  constructor() {
    this.headElements = ['Code', 'Tipo', 'Fecha', 'Dirigido a', 'Acción']
  }
  editarPendientes(item) {
    
  }

  ngOnInit() {
    this.pendientes = [
      { id: 1, code: '001', tipo: 'Solicitud', fechaPendiente: '17/07/2019', dirigidoA: 'Decano', acction: false },
      { id: 2, code: '002', tipo: 'Carta', fechaPendiente: '19/07/2019', dirigidoA: 'Conse de Facultad', acction: false },
      { id: 3, code: '003', tipo: 'Solicitud', fechaPendiente: '20/07/2019', dirigidoA: 'Decano', acction: false },
      { id: 3, code: '004', tipo: 'Memorando', fechaPendiente: '20/07/2019', dirigidoA: 'Conse de Facultad', acction: false },
      { id: 5, code: '005', tipo: 'Solicitud', fechaPendiente: '20/07/2019', dirigidoA: 'Decano', acction: false },
    ];
  }

}
